package in.sunilpaulmathew.izzyondroid.utils;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.widget.ProgressBar;

import com.google.android.material.textview.MaterialTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import in.sunilpaulmathew.izzyondroid.R;
import in.sunilpaulmathew.sCommon.FileUtils.sFileUtils;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on August 19, 2021
 */
public class PackageData {

    public static List<RecyclerViewItems> getRawData(ProgressBar progressBar) {
        JSONArray mPhoneScreenshots = null;
        List<RecyclerViewItems> mData = new ArrayList<>();
        String mTitle, mImageUrl;
        try {
            for (int i = 0; i < Objects.requireNonNull(getApps()).length(); i++) {
                JSONObject apps = Objects.requireNonNull(getApps()).getJSONObject(i);
                if (getAppName(apps) != null) {
                    mTitle = getAppName(apps);
                } else {
                    mTitle = getName(apps);
                }
                if (getIcon(apps) != null) {
                    mImageUrl = "https://apt.izzysoft.de/fdroid/repo/icons/" + getIcon(apps);
                } else {
                    mImageUrl = "https://android.izzysoft.de/frepo/" + getAppPackageName(apps) + "/en-US/icon.png";
                }
                if (getScreenshots(apps) != null) {
                    mPhoneScreenshots = getScreenshots(apps);
                }
                mData.add(
                        new RecyclerViewItems(
                                apps,
                                getAuthorName(apps),
                                getAuthorEmail(apps),
                                getAuthorWebsite(apps),
                                getAppDescription(apps),
                                getDonationLink(apps),
                                getLicense(apps),
                                getAddedDate(apps),
                                getLastUpdated(apps),
                                getAppPackageName(apps),
                                getSourceCode(apps),
                                getSummary(apps),
                                mTitle,
                                mImageUrl,
                                getSuggestedVersion(apps),
                                getChangeLogs(apps),
                                mPhoneScreenshots
                        )
                );
                if (progressBar != null) {
                    progressBar.setMax(getApps().length());
                    if (progressBar.getProgress() < getApps().length()) {
                        progressBar.setProgress(progressBar.getProgress() + 1);
                    } else {
                        progressBar.setProgress(0);
                    }
                }
            }
        } catch (JSONException | NullPointerException ignored) {}
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Collections.sort(mData, (lhs, rhs) -> Long.compare(rhs.getLastUpdated(), lhs.getLastUpdated()));
        }
        return mData;
    }

    public static boolean isCategoryApp(String category, JSONArray array) {
        for (int i = 0; i < array.length(); i++) {
            try {
                if (array.get(i).equals(category)) {
                    return true;
                }
            } catch (JSONException ignored) {}
        }
        return false;
    }

    private static JSONArray getApps() {
        try {
            return Common.getJSONObject().getJSONArray("apps");
        } catch (JSONException ignored) {}
        return null;
    }

    static JSONArray getCategories(JSONObject jsonObject) {
        try {
            return jsonObject.getJSONArray("categories");
        } catch (JSONException ignored) {}
        return null;
    }

    private static long getAddedDate(JSONObject jsonObject) {
        try {
            return jsonObject.getLong("added");
        } catch (JSONException ignored) {}
        return Long.MIN_VALUE;
    }

    private static String getDonationLink(JSONObject jsonObject) {
        try {
            return jsonObject.getString("donate");
        } catch (JSONException ignored) {}
        return null;
    }

    private static long getLastUpdated(JSONObject jsonObject) {
        try {
            return jsonObject.getLong("lastUpdated");
        } catch (JSONException ignored) {}
        return Long.MIN_VALUE;
    }

    private static JSONArray getScreenshots(JSONObject jsonObject) {
        try {
            return Objects.requireNonNull(getEnglish(jsonObject)).getJSONArray("phoneScreenshots");
        } catch (JSONException ignored) {}
        return null;
    }

    private static String getSourceCode(JSONObject jsonObject) {
        try {
            return jsonObject.getString("sourceCode");
        } catch (JSONException ignored) {}
        return null;
    }

    private static String getSummary(JSONObject jsonObject) {
        try {
            return Objects.requireNonNull(getEnglish(jsonObject)).getString("summary");
        } catch (JSONException ignored) {}
        return null;
    }

    private static String getChangeLogs(JSONObject jsonObject) {
        try {
            return Objects.requireNonNull(getEnglish(jsonObject)).getString("whatsNew");
        } catch (JSONException ignored) {}
        return null;
    }

    private static String getAppDescription(JSONObject jsonObject) {
        try {
            return Objects.requireNonNull(getEnglish(jsonObject)).getString("description");
        } catch (JSONException ignored) {}
        return null;
    }

    private static String getSuggestedVersion(JSONObject jsonObject) {
        try {
            return jsonObject.getString("suggestedVersionCode");
        } catch (JSONException ignored) {}
        return null;
    }

    private static String getAppName(JSONObject jsonObject) {
        try {
            return jsonObject.getString("name");
        } catch (JSONException ignored) {}
        return null;
    }

    private static String getAuthorName(JSONObject jsonObject) {
        try {
            return jsonObject.getString("authorName");
        } catch (JSONException ignored) {}
        return null;
    }

    private static String getAuthorEmail(JSONObject jsonObject) {
        try {
            return jsonObject.getString("authorEmail");
        } catch (JSONException ignored) {}
        return null;
    }

    private static String getAuthorWebsite(JSONObject jsonObject) {
        try {
            return jsonObject.getString("authorWebSite");
        } catch (JSONException ignored) {}
        return null;
    }

    private static String getLicense(JSONObject jsonObject) {
        try {
            return jsonObject.getString("license");
        } catch (JSONException ignored) {}
        return null;
    }

    private static String getAppPackageName(JSONObject jsonObject) {
        try {
            return jsonObject.getString("packageName");
        } catch (JSONException ignored) {}
        return null;
    }

    private static String getIcon(JSONObject jsonObject) {
        try {
            return jsonObject.getString("icon");
        } catch (JSONException ignored) {}
        return null;
    }

    private static JSONObject getLocalizations(JSONObject jsonObject) {
        try {
            return jsonObject.getJSONObject("localized");
        } catch (JSONException ignored) {}
        return null;
    }

    private static JSONObject getEnglish(JSONObject jsonObject) {
        try {
            return Objects.requireNonNull(getLocalizations(jsonObject)).getJSONObject("en-US");
        } catch (JSONException ignored) {}
        return null;
    }

    private static String getName(JSONObject jsonObject) {
        try {
            return Objects.requireNonNull(getEnglish(jsonObject)).getString("name");
        } catch (JSONException ignored) {}
        return null;
    }

    public static void acquireRepoData(boolean manualUpdate, MaterialTextView textView, ProgressBar progressBar, Context context) {
        if (manualUpdate || Common.isUpdateTime(context)) {
            try (FileOutputStream output = new FileOutputStream(Common.getIndexFile(context))) {
                URL repoURL = new URL("https://gitlab.com/IzzyOnDroid/iodrepo_binary_transparency_log/-/raw/master/repo/index-v1.json?inline=false");
                URLConnection connection = repoURL.openConnection();
                connection.connect();
                if (progressBar != null) {
                    progressBar.setMax(connection.getContentLength());
                }
                InputStream inputStream = repoURL.openStream();
                byte[] data = new byte[4096];
                int count;
                while ((count = inputStream.read(data)) != -1) {
                    output.write(data, 0, count);
                    if (progressBar != null) {
                        progressBar.setProgress((int) Common.getIndexFile(context).length());
                    }
                }
                Common.setJSONObject(sFileUtils.read(Common.getIndexFile(context)));
                if (textView != null) {
                    new Handler(Looper.getMainLooper()).post(() -> textView.setText(context.getString(R.string.loading)));
                }
                Common.setRawData(getRawData(progressBar));
            } catch (IOException ignored) {
            }
            AppSettings.configureUpdateCheck(context);
        } else {
            Common.setJSONObject(sFileUtils.read(Common.getIndexFile(context)));
            Common.setRawData(getRawData(progressBar));
        }
    }

}