package in.sunilpaulmathew.izzyondroid.utils.tasks;

import android.app.Activity;
import android.view.View;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.RecyclerView;

import in.sunilpaulmathew.izzyondroid.adapters.LatestAppsAdapter;
import in.sunilpaulmathew.izzyondroid.utils.Common;
import in.sunilpaulmathew.izzyondroid.utils.RecyclerViewData;
import in.sunilpaulmathew.sCommon.CommonUtils.sExecutor;
import in.sunilpaulmathew.sCommon.FileUtils.sFileUtils;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on February 18, 2023
 */
public class LatestAppsLoadingTask extends sExecutor {

    private final Activity mActivity;
    private final ProgressBar mProgressBar;
    private final RecyclerView mRecyclerView;

    public LatestAppsLoadingTask(ProgressBar progressBar, RecyclerView recyclerView, Activity activity) {
        mProgressBar = progressBar;
        mRecyclerView = recyclerView;
        mActivity = activity;
    }

    @Override
    public void onPreExecute() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void doInBackground() {
        if (Common.getJSONObject() == null) {
            Common.setJSONObject(sFileUtils.read(Common.getIndexFile(mActivity)));
        }
    }

    @Override
    public void onPostExecute() {
        mRecyclerView.setAdapter(new LatestAppsAdapter(RecyclerViewData.getLatest(mActivity)));
        mProgressBar.setVisibility(View.GONE);
    }

}