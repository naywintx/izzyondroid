package in.sunilpaulmathew.izzyondroid.adapters;

import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textview.MaterialTextView;

import java.util.List;

import in.sunilpaulmathew.izzyondroid.R;
import in.sunilpaulmathew.izzyondroid.activities.CategoriesActivity;
import in.sunilpaulmathew.izzyondroid.utils.CategoryMenuItems;
import in.sunilpaulmathew.izzyondroid.utils.Common;
import in.sunilpaulmathew.sCommon.ThemeUtils.sThemeUtils;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on August 19, 2021
 */
public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    private static List<CategoryMenuItems> data;
    public CategoryAdapter(List<CategoryMenuItems> data){
        CategoryAdapter.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_category_apps, parent, false);
        return new ViewHolder(rowItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            holder.mName.setText(data.get(position).getTitle());
            holder.mIcon.setImageDrawable(data.get(position).getIcon());
            holder.mName.setTextColor(data.get(position).getColor());
            holder.mIcon.setColorFilter(data.get(position).getColor());
            if (!sThemeUtils.isDarkTheme(holder.mMainCard.getContext())) {
                holder.mMainCard.setCardBackgroundColor(Color.LTGRAY);
            }
        } catch (IndexOutOfBoundsException | NullPointerException ignored) {}
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final AppCompatImageView mIcon;
        private final MaterialTextView mName;
        private final MaterialCardView mMainCard;

        public ViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            this.mIcon = view.findViewById(R.id.icon);
            this.mMainCard = view.findViewById(R.id.card_main);
            this.mName = view.findViewById(R.id.name);
        }

        @Override
        public void onClick(View view) {
            Common.setTabPosition(getAdapterPosition());
            Intent details = new Intent(view.getContext(), CategoriesActivity.class);
            view.getContext().startActivity(details);
        }
    }

}