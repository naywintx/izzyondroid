package in.sunilpaulmathew.izzyondroid.adapters;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textview.MaterialTextView;
import com.squareup.picasso.Picasso;

import java.util.List;

import in.sunilpaulmathew.izzyondroid.R;
import in.sunilpaulmathew.izzyondroid.utils.Common;
import in.sunilpaulmathew.izzyondroid.utils.RecyclerViewItems;
import in.sunilpaulmathew.sCommon.ThemeUtils.sThemeUtils;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on August 19, 2021
 */
public class CategoryAppsAdapter extends RecyclerView.Adapter<CategoryAppsAdapter.ViewHolder> {

    private static List<RecyclerViewItems> data;
    public CategoryAppsAdapter(List<RecyclerViewItems> data){
        CategoryAppsAdapter.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_category_apps, parent, false);
        return new ViewHolder(rowItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            holder.mName.setText(data.get(position).getTitle());
            Picasso.get().load(data.get(position).getImageUrl()).placeholder(R.drawable.ic_android).into(holder.mIcon);
            if (!sThemeUtils.isDarkTheme(holder.mMainCard.getContext())) {
                holder.mMainCard.setCardBackgroundColor(Color.LTGRAY);
            }
        } catch (IndexOutOfBoundsException | NullPointerException ignored) {}
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final AppCompatImageView mIcon;
        private final MaterialTextView mName;
        private final MaterialCardView mMainCard;

        public ViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            this.mIcon = view.findViewById(R.id.icon);
            this.mMainCard = view.findViewById(R.id.card_main);
            this.mName = view.findViewById(R.id.name);
        }

        @Override
        public void onClick(View view) {
            try {
                Common.launchPackageView(data.get(getAdapterPosition()), view.getContext());
            } catch (IndexOutOfBoundsException ignored) {}
        }
    }

}